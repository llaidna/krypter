import java.util.Random;
import matrix.*;

/*
algoritmid krüpteermine
43:15 @ https://echo360.e-ope.ee/ess/echo/presentation/00425bc1-4256-4920-9130-76cf99188706
juhuslik (random) maatriks peab olema sama milleks jagasime… random maatriks ei tohi singulaarne olla, ehk determinant ei tohi olla null!
http://www.theasciicode.com.ar/ascii-control-characters/acknowledgement-spade-card-suit-ascii-code-6.html
https://stackoverflow.com/questions/2626835/is-there-functionality-to-generate-a-random-character-in-java
https://stackoverflow.com/questions/16458564/convert-string-to-ascii-value-in-java
https://stackoverflow.com/questions/16602350/calculating-matrix-determinant
https://stackoverflow.com/questions/1992638/java-inverse-matrix-calculation
http://www.math.nyu.edu/~neylon/linalgfall04/project1/jja/group7.htm
http://la4j.org/
https://commons.apache.org/proper/commons-math/userguide/linear.html
http://www.codeproject.com/Articles/405128/Matrix-operations-in-Java
https://stackoverflow.com/questions/8235076/how-to-round-double-to-nearest-whole-number-and-then-convert-to-a-float

tarvis pöördmaatriksit (saab ainult ruutmaatriksil olla)! millega hiljem dekodeerimiseks korrutame jälle
random array determinant ei tohi olla null! kuna ei saa pöördmaatriksit leida...

1. jagada sisendarvud kolmikuteks
 */
//		String[] sisend = new String[]{"t","e","r","e"," ","t","e","e","t"}; // https://stackoverflow.com/questions/1200621/declare-array-in-java
//		String string = "KOHTUME Eile Meil";

public class Krypter {
	public static void main(String[] args) throws NoSquareException {
		
		int mituArgs = args.length; // mitu sõna
		System.out.println("mituArgs: " + mituArgs); // mitu sõna
		double[][] random = new double[3][3]; // krüptimise random massiiv
		
		// kui pole sisestatud ühtegi argumenti, kuvame 6petuse
		if (args.length == 0) {
			System.out.println("Kui soovid sõnumit saatmiseks krüpteerida, sisesta argument e ja seejärel sõnum (ainult ASCII kood).");
			System.out.println("Näide: java Krypter e Täna on ilus ilm, pakk on peidetud kolmanda kivi alla.");
			System.out.println();
			System.out.println("Kui soovid saabunud sõnumit dekrüpteerida, sisesta argument d, seejärel endale teadaoleva võti, siis krüpteeritud sõnum.");
			System.out.println("Näide: java Krypter d 11 22 33 44 55 66 77 88 99 123 123 123 123 123 123 123 123 123 123 123 123 123 123 123 123");
			return;
		}
		
		// kas encrypt või decrypt
		boolean encrypt;
		
		if (args[0].equals("e")) {
			encrypt = true;
			System.out.println("lets encrypt!");
		} else if (args[0].equals("d")) {
			encrypt = false;
			System.out.println("lets decrypt!");
		} else {
			System.out.println("sisestuse viga!");
			System.out.println("Kui soovid sõnumit saatmiseks krüpteerida, sisesta argument e ja seejärel sõnum (ainult ASCII kood).");
			System.out.println("Näide: java Krypter e Täna on ilus ilm, pakk on peidetud kolmanda kivi alla.");
			System.out.println();
			System.out.println("Kui soovid saabunud sõnumit dekrüpteerida, sisesta argument d, seejärel endale teadaoleva võti, siis krüpteeritud sõnum.");
			System.out.println("Näide: java Krypter d 11 22 33 44 55 66 77 88 99 123 123 123 123 123 123 123 123 123 123 123 123 123 123 123 123");
			return;
		}
		
		// kui decryptime
		if (encrypt = false) {
			for (int i = 1; i < 9; i++) {
				
			}
		}

		System.out.println("args[1] " + args[1]);
		String string = "";
		for (int i = 1; i < args.length; i++) {
			string = string + args[i] + " ";
		}

		// mitu tähemärki sisestati, kui pikkus ei jagu kolmega, lisatakse lõppu tühikuid kuni jagub kolmega
		int sisendPikkus = string.length();
		while (sisendPikkus%3 != 0) {
			string = string + " ";
			sisendPikkus = string.length();
		}
		
		String[] sisend = new String[sisendPikkus]; // https://stackoverflow.com/questions/1200621/declare-array-in-java
		sisend = string.split("");
		
		// krüptimiseks random massiiv
		double ajut;
		Random r = new Random();
		for (int i = 0; i < random.length; i++) {
			for (int j = 0; j < random[i].length; j++) {
				ajut = 0 + (9 + 0) * r.nextDouble(); //rangeMin + (rangeMax - rangeMin) * r.nextDouble()
				random[i][j] = (float)Math.round(ajut); 
//				System.out.println("random arv " + (int)random[i][j]);
			}
		}
		
		// Matrix tüüpi asi random massiivist
		Matrix inputMatrix = new Matrix(random);
		
		// determinant ei tohi olla 0. Kui on leiab uue kodeerimise maatriksi, kuni determinant ei ole 0
		int determinant = (int)(MatrixMathematics.determinant(inputMatrix));
		while (determinant == 0) {
			for (int i = 0; i < random.length; i++) {
				for (int j = 0; j < random[i].length; j++) {
					ajut = 0 + (9 + 0) * r.nextDouble(); //rangeMin + (rangeMax - rangeMin) * r.nextDouble()
					random[i][j] = (float)Math.round(ajut); 
				}
			}
		determinant = (int)(MatrixMathematics.determinant(inputMatrix));
		}

		System.out.println("determinant " + determinant);
		
		// Matrix tüüpi inverseMatrix
		Matrix inverseMatrix = MatrixMathematics.inverse(inputMatrix);
		
		// d 8 2 1 4 5 7 0 5 7 1244 1767 1303 1270 1809 1337 1267 1750 1282 979 1365 965 1070 1573 1185 1028 1162 774 1235 1720 1260 1026 853 389
		
		// teeb massiivi sisendi ascii koodide jaoks
		double[] sisendAscii = new double[sisendPikkus];

		// teeb massiivi kodeeritud koodide jaoks
		double[] encoded = new double[sisendPikkus]; 

		// teeb massiivi dekodeeritud koodide jaoks
		double[] decoded = new double[sisendPikkus]; 
		
		// sisendAscii
		for (int i = 0; i < sisendPikkus; i++) {
				sisendAscii[i] = sisend[i].charAt(0);
//				System.out.println("charAt: " + sisend[i].charAt(0) + " ascii: " + sisendAscii[i]);
		}
		
		System.out.println("sisendi pikkus: " + sisendPikkus + " tähemärki");
		System.out.print("sisend on:      ");
		for (int i = 0; i < sisend.length; i++) {
			System.out.print(sisend[i]);
		}
		

		// prindime kodeerimise inputMatrix maatriksi
		System.out.println();
		System.out.println("kodeerimise inputMatrix maatriks, ehk VÕTI on: ");
		for (int i = 0; i < 3; i++) {
			for (int j = 0; j < 3; j++) {
				System.out.print((int)inputMatrix.getValueAt(i, j) + " ");
			}
		}

		
		// prindime kodeerimise "sisend" maatriksi
		System.out.println();
		System.out.print("sisend massiiv on: ");
		for (int i = 0; i < sisendPikkus; i++) {
				System.out.print(sisend[i]);
		}
		
		
		// prindime sisendAscii maatriksi
		System.out.println();
		System.out.print("sisendAscii on:    ");
		for (int i = 0; i < sisendPikkus; i++) {
				System.out.print((int)sisendAscii[i] + " ");
		}
		System.out.println();
		
		
		double vahetulemus = 0;
		int luger = 0;
		
		// encode
		System.out.print("kodeeritud sõnum: ");
		for (int i = 0; i < sisendPikkus/3; i++) {
			for (int j = 0; j < 3; j++) {
				for (int k = 0; k < 3; k++) {
					vahetulemus = vahetulemus + inputMatrix.getValueAt(j % 3, k) * sisendAscii[k+(i*3)];
				}
				encoded[luger] = vahetulemus;
				luger++;
//				System.out.println("encoded[i] = " + luger + " = " + (int)vahetulemus);
				System.out.print((int)vahetulemus + " ");
				vahetulemus = 0;
			}
		}

		// decode
		System.out.println();
		System.out.print("dekodeeritud sõnum: ");
		luger = 0;
		for (int i = 0; i < sisendPikkus/3; i++) {
			for (int j = 0; j < 3; j++) {
				for (int k = 0; k < 3; k++) {
					vahetulemus = vahetulemus + inverseMatrix.getValueAt(j % 3, k) * encoded[k+(i*3)];
				}
				decoded[luger] = vahetulemus;
				luger++;
//				System.out.println("decoded[i] = " + luger + " = " + (int)Math.round(vahetulemus) + " " + (char)(float)Math.round(vahetulemus));
				System.out.print((char)(float)Math.round(vahetulemus));
				vahetulemus = 0;
			}
		}
		
	}
	
}

// prindime kodeerimise (random) maatriksi
//		System.out.println();
//		System.out.println("kodeerimise (random) maatriks on: ");
//		for (int i = 0; i < 3; i++) {
//			for (int j = 0; j < 3; j++) {
//				System.out.print(random[i][j] + " ");
//			}
//			System.out.println(); // reavahetus
//		}

//		// prindime kodeerimise inverseMatrix maatriksi
//		System.out.println();
//		System.out.println("kodeerimise inverseMatrix maatriks on: ");
//		for (int i = 0; i < 3; i++) {
//			for (int j = 0; j < 3; j++) {
//				System.out.print(inverseMatrix.getValueAt(i, j) + " ");
//			}
//			System.out.println(); // reavahetus
//		}

//	
//	public static double arvuta() {
//		
//		return 0;
//	}

//		String[] sisend = new String[]{"K","O","H","T","U","M","E"," ","R"}; // https://stackoverflow.com/questions/1200621/declare-array-in-java

// arvutame determinandi, see ei tohi võrduda 0
//		int determinant = (
//				(random[0][0]*random[1][1]*random[2][2]+random[0][1]*random[1][2]*random[2][0]) - 
//				(random[2][0]*random[1][1]*random[0][2]+random[1][0]*random[0][1]*random[2][2])
//				);
//		System.out.println("determinant: " + determinant);


//		// next 3
//		for (int i = 3; i < 6; i++) {
//				for (int k = 0; k < 3; k++) {
//					vahetulemus = vahetulemus + inputMatrix.getValueAt(i % 3, k) * sisendAscii[k+3];
//				}
//				encoded[i] = vahetulemus;
//				System.out.println("encoded[i] = " + i + " = " + (int)vahetulemus);
//				vahetulemus = 0;
//		}
//		// next 3
//		for (int i = 6; i < 9; i++) {
//				for (int k = 0; k < 3; k++) {
//					vahetulemus = vahetulemus + inputMatrix.getValueAt(i % 3, k) * sisendAscii[k+6];
//				}
//				encoded[i] = vahetulemus;
//				System.out.println("encoded[i] = " + i + " = " + (int)vahetulemus);
//				vahetulemus = 0;
//		}
//		// decode 3 järgmist
//		for (int i = 3; i < 6; i++) {
//				for (int k = 0; k < 3; k++) {
//					vahetulemus = vahetulemus + inverseMatrix.getValueAt(i % 3, k) * encoded[k+3];
//				}
//				decoded[i] = vahetulemus;
//				System.out.println("decoded[i] = " + i + " = " + (int)vahetulemus + " " + (char)vahetulemus);
//				vahetulemus = 0;
//		}
//		// decode 3 järgmist
//		for (int i = 6; i < 9; i++) {
//				for (int k = 0; k < 3; k++) {
//					vahetulemus = vahetulemus + inverseMatrix.getValueAt(i % 3, k) * encoded[k+6];
//				}
//				decoded[i] = vahetulemus;
//				System.out.println("decoded[i] = " + i + " = " + (int)vahetulemus + " " + (char)vahetulemus);
//				vahetulemus = 0;
//		}



//		for (int i = 0; i < sisendPikkus; i++) {
//			for (int j = 0; j < 3; j++) {
//				vahetulemus = vahetulemus + inverseMatrix.getValueAt(i%3, j) * encoded[j];
//			}
//			decoded[i] = vahetulemus;
//			System.out.println("decoded[i] = " + i + " = " + vahetulemus);
//			vahetulemus = 0;
//		}

// krüptimiseks käsitsi massiiv
//		double[][] random = new double[][]{	{  1, -5, 10 },
//						   					{ 11, -6,  5 },
//						   					{ -9, 15,  4 }};
//		// kontrollarvud maatriksist
//		System.out.println("inputMatrix(0)(0): " + inputMatrix.getValueAt(1, 1));
//		System.out.println("inverseMatrix(0)(0): " + inverseMatrix.getValueAt(1, 1));